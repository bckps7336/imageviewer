package com.bckps7336.imageviewer;

import android.app.Fragment;
import android.os.Bundle;

import org.json.JSONObject;

import java.util.List;

public class RetainedFragment extends Fragment {
    // data object we want to retain
    private List<JSONObject> data;

    // this method is only called once for this fragment
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // retain this fragment
        setRetainInstance(true);
    }

    public List<JSONObject> getData() {
        return data;
    }

    public void setData(List<JSONObject> data) {
        this.data = data;
    }
}
