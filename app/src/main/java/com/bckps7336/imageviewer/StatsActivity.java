package com.bckps7336.imageviewer;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.StatsSnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class StatsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stats_main);

        ListView lv = findViewById(R.id.statView);

        List<HashMap<String, String>> list = new ArrayList<>();

        StatsSnapshot ss = Picasso.get().getSnapshot();

        String[] title = {"Cache usage", "Version"};
        String[] value = {Utils.byteFormat(ss.size) + " / " + Utils.byteFormat(ss.maxSize) + " (" + Math.ceil((float) ss.size / ss.maxSize * 100) + "% Full)", Constant.VERSION};

        for (int i = 0; i < title.length; i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("title", title[i]);
            hashMap.put("value", value[i]);

            list.add(hashMap);
        }

        SimpleAdapter adapter = new SimpleAdapter(this,
                list,
                android.R.layout.simple_list_item_2,
                new String[]{"title", "value"},
                new int[]{android.R.id.text1, android.R.id.text2}) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                TextView tv1 = view.findViewById(android.R.id.text1);
                TextView tv2 = view.findViewById(android.R.id.text2);

                tv1.setTextColor(Color.parseColor("#FCFCFC"));
                tv2.setTextColor(Color.parseColor("#F0F0F0"));

                return view;
            }
        };

        lv.setAdapter(adapter);
    }
}
