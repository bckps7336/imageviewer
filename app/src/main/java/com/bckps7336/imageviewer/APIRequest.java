package com.bckps7336.imageviewer;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class APIRequest {
    private static OkHttpClient client = new OkHttpClient();

    private static List<JSONObject> parseJson (String data) {
        try {
            JSONArray arr = new JSONArray(data);
            List<JSONObject> out = new ArrayList<>();

            for (int i = 0; i < arr.length(); i++) {
                out.add(arr.getJSONObject(i));
            }

            return out;
        } catch (JSONException e) {
            Log.d("parseJSON", e.getMessage() + "\n" + data);
            return null;
        }
    }

    private static JSONObject createObj (String provider, JSONObject ori) {
        JSONObject out = new JSONObject();
        Date _time; String time;

        try {
            String oriTime = ori.get(Constant.API_KEY_COMMON.get("ts")).toString();
            switch (provider) {
                case "dan":
                    _time = Constant.SDF.get("dan").parse(oriTime);
                    break;
                case "gel":
                    _time = Constant.SDF.get("gel").parse(oriTime);
                    break;
                default:
                    _time = new Date((long) Integer.parseInt(oriTime) * 1000);
            }

            time = Constant.SDF_FORMAT.format(_time);

            if (provider.equals("gel")) { // Self-construct thumbnail
                ori.put("thumbnail", "https://simg3.gelbooru.com/thumbnails/" + ori.getString("directory") + "/thumbnail_" + ori.getString("image").split("\\.")[0] + ".jpg");
            }

            for (Map.Entry<String, String> e : Constant.API_KEY_COMMON.entrySet()) {
                switch (e.getKey()) {
                    case "rating":
                        out.put("rating", Constant.RATING.get(ori.getString(e.getValue())));
                        break;
                    case "ts":
                        out.put("ts", time);
                        break;
                    case "size":
                        if (provider == "gel") {
                            out.put("size", -1);
                        } else {
                            out.put("size", ori.get(e.getValue()));
                        }
                        break;
                    default:
                        out.put(e.getKey(), ori.get(e.getValue()));
                        break;
                }
            }

            for (Map.Entry<String, Map<String, String>> e : Constant.API_KEY_SPECIAL.entrySet()) {
                out.put(e.getKey(), ori.get(e.getValue().get(provider)));
            }
            return out;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("createObj", e.getMessage() + "\n" + ori.toString());
            return out;
        }
    }

    public static List<JSONObject> fetchData(String provider, int page, List<String> tags, boolean safe) {
        try {
            HttpUrl.Builder _request;
            Request request;
            Response response;
            List<JSONObject> imageList;

            List<JSONObject> output = new ArrayList<>();

            if (safe) {
                tags.add("rating:safe");
            }


            _request = HttpUrl.parse(Constant.API.get(provider)).newBuilder();

            _request.addQueryParameter("tags", TextUtils.join(" ", tags));
            _request.addQueryParameter("limit", String.valueOf(100));

            if (provider.equals("gel")) {
                _request.addQueryParameter("pid", String.valueOf(page - 1));
            } else {
                _request.addQueryParameter("page", String.valueOf(page));
            }

            Log.d("req", _request.build().toString());

            request = new Request.Builder().url(_request.build()).build();

            response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                return null;
            }

            imageList = parseJson(response.body().string());

            for (int i = 0; i < imageList.size(); i++) {
                output.add(createObj(provider, imageList.get(i)));
            }
            return output;

        } catch (Exception e) {
            Log.d("fetchData", "error");
            e.printStackTrace();
            return null;
        }
    }

    public static List<JSONObject> fetchData(String provider, int page) { // default
        return fetchData(provider, page, new ArrayList<String>(), true);
    }
}
