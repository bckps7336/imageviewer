// TODO: Download option
// TODO: Basic dynamic layout
// TODO: Basic info screen after user interact
// TODO: Donate and user info
// TODO: Save image to local
// TODO: Share image
// TODO: Uniform-ize APIs
// TODO: User local favs w/ export & import

package com.bckps7336.imageviewer;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.github.clans.fab.FloatingActionMenu;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ExecutorService service = Executors.newSingleThreadExecutor();

    private RecyclerView rvMain;
    private ProgressBar pb;

    private FloatingActionMenu fam;
    private com.github.clans.fab.FloatingActionButton fab_safe;
    private DataAdapter adapter;

    private RetainedFragment dataFragment;

    private Picasso picasso;

    private List<JSONObject> imageList;

    private String provider = "kona";
    private int page = 1;

    private boolean isLoading = false;
    private boolean isSafe = true;

    private void load(final RecyclerView view) {
        try {
            DisplayMetrics dm = new DisplayMetrics();
            ((WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(dm);

            double dpWidth = dm.widthPixels / getResources().getDisplayMetrics().density;
            final double a = Math.max(0, dpWidth % 120 - (dpWidth / 120 * 8));

            Log.d("left", String.valueOf(dpWidth % 120));

            if (imageList == null) {
                Snackbar.make(view, "Error occured: Empty image list", Snackbar.LENGTH_LONG);
                return;
            }

            adapter = new DataAdapter(this, getApplicationContext(), imageList);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rvMain.setPadding((int) ((a / 2) * getResources().getDisplayMetrics().density), 0, (int) ((a / 2) * getResources().getDisplayMetrics().density), 0);
                    view.setAdapter(adapter);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fam = findViewById(R.id.fam);
        fab_safe = findViewById(R.id.fab_safe);

        fab_safe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fam.close(true);
                Snackbar.make(view, "Toggle Safe mode?", Snackbar.LENGTH_LONG)
                        .setAction("YES", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                isSafe = !isSafe;
                            }
                        }).show();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final SwipeRefreshLayout srl = findViewById(R.id.srl);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                service.submit(new Runnable() {
                    @Override
                    public void run() {
                        imageList = APIRequest.fetchData(provider, 1, new ArrayList<String>(), isSafe);
                        dataFragment.setData(imageList);
                        load(rvMain);
                        Log.d("load", "done");
                        srl.post(new Runnable() {
                            @Override
                            public void run() {
                                srl.setRefreshing(false);
                            }
                        });
                    }
                });
            }
        });

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fm = getFragmentManager();
        dataFragment = (RetainedFragment) fm.findFragmentByTag("imageList");

        if (dataFragment == null) {
            final ProgressListener progressListener = new ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    int progress = (int) ((100 * bytesRead) / contentLength);

                    // Enable if you want to see the progress with logcat
                    // Log.v(LOG_TAG, "Progress: " + progress + "%");
                    //progressBar.setProgress(progress);
                    try {
                        //ImageActivity.pb.setMax(100);
                        ImageActivity.pb.setProgress(50);
                        Log.d("progress", String.valueOf(progress));
                    } catch (Exception e) {

                    }
                }
            };

            OkHttpClient ohc = new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Log.d("chain", chain.request().url().toString());
                            Response originalResponse = chain.proceed(chain.request());
                            return originalResponse.newBuilder()
                                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                                    .build();
                        }
                    })
                    .build();

            Picasso p = new Picasso.Builder(getApplicationContext())
                    .downloader(new OkHttp3Downloader(ohc))
                    .build();
            Picasso.setSingletonInstance(p);
        }

        picasso = Picasso.get();

        pb = findViewById(R.id.pb);

        rvMain = findViewById(R.id.rvMain);
        rvMain.setHasFixedSize(true);

        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(dm);

        double dpWidth = dm.widthPixels / getResources().getDisplayMetrics().density;

        final RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, (int) Math.floor(Math.max(dpWidth / 120, 1)));
        rvMain.setLayoutManager(layoutManager);

        rvMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int currentItem = layoutManager.getChildCount();
                int totalItem = layoutManager.getItemCount();
                int scrollOutItem = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();

                if (Math.abs(dy) < 20) {
                    Log.d("tag", "resume");
                    picasso.resumeTag("image");
                } else {
                    Log.d("tag", "pause");
                    picasso.pauseTag("image");
                }

                if (!isLoading && (currentItem + scrollOutItem >= totalItem - 20)) { // Preload
                    Log.d("status", "ScrollEnd");
                    isLoading = true;

                    pb.setVisibility(View.VISIBLE);

                    service.submit(new Runnable() {
                        @Override
                        public void run() {
                            imageList.addAll(APIRequest.fetchData("kona", page++, new ArrayList<String>(), isSafe));
                            Log.d("refreshList", imageList.toString());
                            dataFragment.setData(imageList);

                            service.submit(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.notifyDataSetChanged();
                                }
                            });
                            Log.d("refreshList", "done");

                            isLoading = false;
                            pb.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            }
        });

        if (dataFragment == null) {
            dataFragment = new RetainedFragment();
            fm.beginTransaction().add(dataFragment, "imageList").commit();

            service.submit(new Runnable() {
                @Override
                public void run() {
                    imageList = APIRequest.fetchData(provider, page++, new ArrayList<String>(), isSafe);
                    dataFragment.setData(imageList);
                    load(rvMain);
                }
            });

        } else {
            imageList = dataFragment.getData();
            load(rvMain);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // store the data in the fragment
        dataFragment.setData(imageList);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Log.d("click", String.valueOf(id));
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_stats:
                Intent intent = new Intent(this, StatsActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        page = 1;

        service.submit(new Runnable() {
            @Override
            public void run() {
                // Handle navigation view item clicks here.
                int id = item.getItemId();
                switch (id) {
                    case R.id.navKona:
                        provider = "kona";
                        break;
                    case R.id.navYan:
                        provider = "yan";
                        break;
                    case R.id.navDan:
                        provider = "dan";
                        break;
                    case R.id.navGel:
                        provider = "gel";
                        break;
                    default:
                        break;
                }
                imageList = APIRequest.fetchData(provider, page++, new ArrayList<String>(), isSafe);
                dataFragment.setData(imageList);
                load(rvMain);
            }
        });

        return true;
    }
}
