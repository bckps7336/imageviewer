package com.bckps7336.imageviewer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;


public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private Context _context;
    private Context context;
    private final int maxLength = 120;
    private List<JSONObject> images;

    public DataAdapter(Context _context, Context context, List<JSONObject> images) {
        this._context = _context;
        this.context = context;
        this.images = images;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        try {
            String txt = "";

            for (Map.Entry<String, String> m : Constant.API_KEY.entrySet()) {
                txt += m.getValue() + ": " + images.get(i).getString(m.getKey()) + "\n\n";
            }

            viewHolder.tn_img.setId(Constant.ID_VIEW_BASE + i);
            viewHolder.tv_imgDim.setId(Constant.ID_VIEW_BASE + i);
            viewHolder.tv_imgSize.setId(Constant.ID_VIEW_BASE + i);
            viewHolder.ll.setId(Constant.ID_VIEW_BASE + i);

            viewHolder.tv_imgDim.setText(images.get(i).getString("lw") + "*" + images.get(i).getString("lh"));
            viewHolder.tv_imgSize.setText(Utils.byteFormat(images.get(i).getInt("size")));

            switch (images.get(i).getString("rating")) {
                case "Safe":
                    viewHolder.tv_imgDim.setBackgroundColor(Color.parseColor("#7df28b"));
                    break;
                case "Questionable":
                    viewHolder.tv_imgDim.setBackgroundColor(Color.parseColor("#e4ea69"));
                    break;
                case "Explicit":
                    viewHolder.tv_imgDim.setBackgroundColor(Color.parseColor("#d37a52"));
                    break;
            }


            Log.d("tn_url", images.get(i).getString("surl"));
            //Picasso.get().setIndicatorsEnabled(true);
            Picasso
                    .get()
                    .load(images.get(i).getString("surl"))
                    .tag("image")
                    .placeholder(R.drawable.progress_ani_sd)
                    .error(R.drawable.ic_error_outline_red_24dp)
                    .into(viewHolder.tn_img);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_imgDim;
        TextView tv_imgSize;
        ImageView tn_img;
        LinearLayout ll;

        View.OnClickListener onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.d("vid", String.valueOf(v.getId()));

                    JSONObject image = images.get(v.getId() - Constant.ID_VIEW_BASE);
                    Intent i = new Intent(v.getContext(), ImageActivity.class);

                    i.putExtra("url", image.getString("lurl"));
                    i.putExtra("id", image.getString("id"));
                    i.putExtra("size", Utils.byteFormat(image.getInt("size")));
                    i.putExtra("src", image.getString("src"));

                    v.getContext().startActivity(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        public ViewHolder(final View view) {
            super(view);

            tv_imgDim = view.findViewById(R.id.tv_imgDim);
            tv_imgSize = view.findViewById(R.id.tv_imgSize);

            tn_img = view.findViewById(R.id.tn_img);
            ll = view.findViewById(R.id.ll);

            ll.setOnClickListener(onClick);
            tn_img.setOnClickListener(onClick);
        }
    }
}