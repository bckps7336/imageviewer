package com.bckps7336.imageviewer;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ortiz.touchview.TouchImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


public class ImageActivity extends AppCompatActivity {
    private String id;
    private String url;
    private String size;
    private String src;

    public static ProgressBar pb;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.image_main);

        Intent intent = getIntent();
        url = intent.getStringExtra("url");
        id = intent.getStringExtra("id");
        size = intent.getStringExtra("size");
        src = intent.getStringExtra("src");

        TextView tv_id = findViewById(R.id.image_id);
        tv_id.setText(Constant.API_KEY.get("id") + ": " + id);

        TextView tv_size = findViewById(R.id.image_size);
        tv_size.setText((size.length() > 0 ? Constant.API_KEY.get("size") + ": " + size : ""));

        TextView tv_imgSrc = findViewById(R.id.image_src);
        String tmp = Utils.parseSrc(src);
        tv_imgSrc.setText(tmp + (tmp.length() > 0 ? ": " : "") + src);
        TouchImageView iv = findViewById(R.id.fullImage);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout ll = findViewById(R.id.image_ll);

                if (ll.getVisibility() == View.VISIBLE) {
                    ll.setVisibility(View.INVISIBLE);
                } else {
                    ll.setVisibility(View.VISIBLE);
                    ll.setAlpha(0f);

                    ll.animate()
                            .alpha(1f)
                            .setDuration(250)
                            .setListener(null);
                }
            }
        });


        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(this);
        circularProgressDrawable.setColorSchemeColors(Color.parseColor("#489127"));
        circularProgressDrawable.setArrowEnabled(true);
        circularProgressDrawable.setArrowDimensions(10f, 10f);
        circularProgressDrawable.setStrokeWidth(10f);
        circularProgressDrawable.setCenterRadius(60f);
        circularProgressDrawable.start();

        Picasso
                .get()
                .load(url)
                .tag("image_big")
                //.placeholder(circularProgressDrawable)
                .error(R.drawable.ic_error_outline_red_24dp)
                .into(iv, new Callback() {
                    @Override
                    public void onSuccess() {
                        pb.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                    }
                });

        Button btn_back = findViewById(R.id.image_btn_back);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button btn_action = findViewById(R.id.image_btn_action);

        btn_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerForContextMenu(v);
                openContextMenu(v);
            }
        });

        Button btn_share = findViewById(R.id.btn_share);

        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, url);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        Button btn_fav = findViewById(R.id.btn_fav);

        btn_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: Add to favourite
            }
        });

        Utils.parseSrc(src);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.image_action, menu);
        if (src.length() == 0) menu.findItem(R.id.img_cpSrcURL).setEnabled(false);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData cd;
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.img_cpImgURL:
                cd = ClipData.newPlainText("imgURL", url);
                cm.setPrimaryClip(cd);
                return true;
            case R.id.img_cpSrcURL:
                cd = ClipData.newPlainText("srcURL", url);
                cm.setPrimaryClip(cd);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
