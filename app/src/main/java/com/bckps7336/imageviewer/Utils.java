package com.bckps7336.imageviewer;

import android.annotation.SuppressLint;
import android.util.Log;

import java.net.URL;
import java.util.List;

public class Utils {
    @SuppressLint("DefaultLocale")
    public static String byteFormat(int byteCount) {
        try {
            if (byteCount == -1) return ""; // Gelbooru

            String[] units = {"B", "K", "M", "G"};
            int index = (int) (Math.log(byteCount) / Math.log(1000)); // log_(1000) byteCount, 1023.99K -> 0.99M
            return String.format("%.2f", (double) byteCount / Math.pow(1024, index)) + units[index];
        } catch (Exception e) {
            e.printStackTrace();
            return String.valueOf(byteCount) + "B";
        }
    }

    public static String parseSrc(String src) {
        Log.d("parseSrc", src);
        try {
            URL u = new URL(src);
            String host = u.getHost();

            switch (host) {
                case "twitter.com":
                case "i.pximg.net":
                    return "Twitter";
                case "www.pixiv.net":
                    return "Pixiv";
            }
            return "";
        } catch (Exception e) {
            return "";
        }


    }

    public List<String> compareTags() {
        return null;
    }
}
