package com.bckps7336.imageviewer;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public final class Constant {
    public static final String VERSION = "Alpha 20180726";

    public static final Map<String, String> PROVIDER; // Name of providers
    public static final Map<String, String> API; // Portals

    public static final Map<String, String> API_KEY; // key-value
    public static final Map<String, Map<String, String>> API_KEY_SPECIAL;
    public static final Map<String, String> API_KEY_COMMON;

    public static final Map<String, SimpleDateFormat> SDF; // SimpleDateFormat

    public static final Map<String, String> SQL_INIT; // onCreate()
    public static final Map<String, Integer> SQL_GEL_TAG_TYPE; // Gelbooru does not have default id <-> tag_name pair

    public static final Map<String, String> RATING;


    static {
        PROVIDER = new HashMap<>();
        PROVIDER.put("kona", "Konachan");
        PROVIDER.put("yan", "Yandere");
        PROVIDER.put("dan", "Danbooru");
        PROVIDER.put("gel", "Gelbooru");

        API = new HashMap<>();
        API.put("kona", "http://konachan.com/post.json");
        API.put("yan", "https://yande.re/post.json");
        API.put("dan", "https://danbooru.donmai.us/posts.json");
        API.put("gel", "https://gelbooru.com/index.php?json=1&page=dapi&s=post&q=index");

        API_KEY = new HashMap<>();

        API_KEY.put("id", "ID");
        API_KEY.put("ts", "Created at");
        API_KEY.put("src", "Source");
        API_KEY.put("rating", "Rating");
        API_KEY.put("size", "Size");

        API_KEY.put("tag", "Tags");
        API_KEY.put("lurl", "Image URL");
        API_KEY.put("lw", "Image Width");
        API_KEY.put("lh", "Image Height");
        API_KEY.put("surl", "Thumbnail URL");

        API_KEY_SPECIAL = new HashMap<>();
        Map<String, String> tag = new HashMap<>();

        tag.put("kona", "tags");
        tag.put("yan", tag.get("kona"));
        tag.put("dan", "tag_string");
        tag.put("gel", "tags");

        API_KEY_SPECIAL.put("tag", tag);

        // LargeFileSize: Gel does not provide, others use `file_size`

        Map<String, String> lurl = new HashMap<>(); // LargeFileURL

        lurl.put("kona", "file_url");
        lurl.put("yan", lurl.get("kona"));
        lurl.put("dan", "large_file_url");
        lurl.put("gel", "file_url");

        API_KEY_SPECIAL.put("lurl", lurl);

        Map<String, String> lw = new HashMap<>(); // LargeFileWidth

        lw.put("kona", "width");
        lw.put("yan", lw.get("kona"));
        lw.put("dan", "image_width");
        lw.put("gel", "width");

        API_KEY_SPECIAL.put("lw", lw);

        Map<String, String> lh = new HashMap<>(); // LargeFileHeight

        lh.put("kona", "height");
        lh.put("yan", lh.get("kona"));
        lh.put("dan", "image_height");
        lh.put("gel", "height");

        API_KEY_SPECIAL.put("lh", lh);

        Map<String, String> surl = new HashMap<>(); // Thumbnail...

        surl.put("kona", "preview_url");
        surl.put("yan", surl.get("kona"));
        surl.put("dan", "preview_file_url");
        surl.put("gel", "thumbnail"); // Self-construct

        API_KEY_SPECIAL.put("surl", surl);

        API_KEY_COMMON = new HashMap<>();

        API_KEY_COMMON.put("id", "id");
        API_KEY_COMMON.put("ts", "created_at");
        API_KEY_COMMON.put("src", "source");
        API_KEY_COMMON.put("rating", "rating");
        API_KEY_COMMON.put("size", "file_size");

        SDF = new HashMap<>();
        SDF.put("dan", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US));
        SDF.put("gel", new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US));

        SQL_INIT = new HashMap<>();
        SQL_INIT.put("init_tag_list", "CREATE TABLE tag_list ( provider INT, id INT, count INT NOT NULL, name VARCHAR(200) NOT NULL, type INT NOT NULL, PRIMARY KEY (provider, id));");
        SQL_INIT.put("init_tag_type", "CREATE TABLE tag_type ( provider INT, id INT, name VARCHAR(10) NOT NULL, PRIMARY KEY (provider, id)); INSERT INTO tag_type VALUES (0, 0, \"General\"), (0, 1, \"Artist\"), (0, 3, \"Copyright\"), (0, 4, \"Character\"), (0, 5, \"Style\"), (0, 6, \"Circle\"), (1, 0, \"General\"), (1, 1, \"Artist\"), (1, 3, \"Copyright\"), (1, 4, \"Character\"), (1, 5, \"Circle\"), (1, 6, \"Faults\"), (2, 0, \"General\"), (2, 1, \"Artist\"), (2, 3, \"Copyright\"), (2, 4, \"Character\"), (3, 0, \"Tag\"), (3, 1, \"Metadata\"), (3, 2, \"Artist\"), (3, 3, \"Copyright\"), (3, 4, \"Character\");");

        SQL_GEL_TAG_TYPE = new HashMap<>();
        SQL_GEL_TAG_TYPE.put("tag", 0);
        SQL_GEL_TAG_TYPE.put("metadata", 1);
        SQL_GEL_TAG_TYPE.put("artist", 2);
        SQL_GEL_TAG_TYPE.put("copyright", 3);
        SQL_GEL_TAG_TYPE.put("character", 4);

        RATING = new HashMap<>();
        RATING.put("s", "Safe");
        RATING.put("q", "Questionable");
        RATING.put("e", "Explicit");
    }

    public static final SimpleDateFormat SDF_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

    public static int ID_VIEW_BASE = 0x7336;
    /*
    Konachan, Yandere:
    {
            --!"id": 268536,
            !"tags": "a.i._channel barefoot bikini dennou_shoujo_youtuber_shiro group kaguya_luna kaguya_luna_(character) kizuna_a.i. mirai_akari mirai_akari_project shiro_(dennou_shoujo_youtuber_shiro) swimsuit tagme tagme_(artist) water watermark",
            --!"created_at": 1532109125,
            --!"source": "https://www.pixiv.net/member_illust.php?mode=medium&illust_id=69780968",
            !"file_size": 1628895,
            !"file_url": "https://konachan.com/image/2a7929e86a3bc58ba215629b1318e8b1/Konachan.com%20-%20268536%20a.i._channel%20barefoot%20bikini%20group%20kaguya_luna%20kizuna_a.i.%20mirai_akari%20mirai_akari_project%20swimsuit%20tagme%20tagme_%28artist%29%20water%20watermark.jpg",
            !"preview_url": "https://konachan.com/data/preview/2a/79/2a7929e86a3bc58ba215629b1318e8b1.jpg",
            ?"actual_preview_width": 300,
            ?"actual_preview_height": 212,
            --!"rating": "s",
            "width": 0,
            "height: 0
    }

    Danbooru:
    {
            --!"id": 3196226,
            --!"created_at": "2018-07-20T14:57:27.577-04:00",
            --!"rating": "s",
            !"image_width": 671,
            !"image_height": 887,
            !"tag_string": "1girl alternate_costume artist_request breasts chain cleavage collar fingerless_gloves fur_collar gae_bolg_(phantom_of_the_kill) gloves green_hair hair_ornament holding holding_spear holding_weapon horns large_breasts long_hair official_art phantom_of_the_kill polearm red_eyes shoulder_armor spear weapon",
            ?"file_size": 106456,
            --!"source": "",
            "large_file_url": "https://danbooru.donmai.us/data/047723afaf75cefb43e133eb6284b352.jpg",
            !"preview_file_url": "https://danbooru.donmai.us/data/preview/047723afaf75cefb43e133eb6284b352.jpg"
    }
    Gelbooru:
    {
            --!"source": "http://www.pixiv.net/member_illust.php?mode=medium&amp;illust_id=69781577",
            ?"height": 3600,
            !"id": 4329973,
            --!"rating": "s",
            ?"sample_height": 1443,
            ?"sample_width": 850,
            !"tags": "1girl absurdres arm_warmers artist_name bare_shoulders black_hair black_legwear black_sash braid cowboy_shot demon_tail demon_wings dress eyebrows_visible_through_hair finger_to_chin hand_up highres horns kijin_seija looking_at_viewer medium_hair nail_polish off-shoulder_dress off_shoulder pantyhose petticoat red_hair red_nails sash sheya signature simple_background smile solo standing tail touhou white_background white_hair wings",
            ?"width": 2120,
            !"file_url": "https://simg3.gelbooru.com/images/0b/a3/0ba335f16211b87123c26032ca235b4b.jpg",
            --!"created_at": "Fri Jul 20 12:32:29 -0500 2018",
            *!"thunbnail": `s/images/thumbnails/`
    }
     */
}
